const mongoose = require('mongoose')
const { ObjectId } = mongoose.Schema.Types
const Schema = mongoose.Schema

const userSchema = new Schema({
    email: { type: String },
    username: { type: String},
    mobile: { type: String },
    password: { type: String },
    reset_password_token: { type: String },
    reset_password_expires: { type: Date },
}, { timestamps: true });


module.exports = mongoose.model('user', userSchema)