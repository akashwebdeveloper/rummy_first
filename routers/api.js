const router = require('express').Router();
const { signup, login, forgetPassword, reset_password,post_reset_password,success, verify, register, register1, resendotp } = require('../controller/auth/authController')

router.post('/signup', signup)
router.post('/login', login)
router.post('/forgetPassword', forgetPassword)
router.get('/reset_password', reset_password)
router.post('/post_reset_password', post_reset_password)


router.get('/success', success)
module.exports = router;